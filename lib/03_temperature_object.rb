class Temperature
  # TODO: your code goes here!
  def initialize(options)
    @fahrenheit = options[:f]
    @celsius = options[:c]
  end

  def self.from_fahrenheit(temp)
    self.new(f: temp)
  end

  def self.from_celsius(temp)
    self.new(c: temp)
  end

  def in_fahrenheit
    @fahrenheit.nil? ? self.class.ctof(@celsius) : @fahrenheit
  end

  def in_celsius
    @celsius.nil? ? self.class.ftoc(@fahrenheit) : @celsius
  end

  def self.ftoc(temp)
    (temp.to_f - 32) * 5 / 9.0
  end

  def self.ctof(temp)
    temp.to_f * 9 / 5.0 + 32
  end
end


class Celsius < Temperature
  def initialize(temp)
    @celsius = temp
  end
end

class Fahrenheit < Temperature
  def initialize(temp)
    @fahrenheit = temp
  end
end
