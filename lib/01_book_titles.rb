class Book
  # TODO: your code goes here!
  OMIT = ["a", "an", "the", "in", "of", "and"]

  attr_reader :title

  def title=(title)
    # @title = title
    words = title.split.map(&:downcase)
    formated = words.map.with_index do |word, idx|
      if OMIT.include?(word) && idx != 0
        word
      else
        word.capitalize
      end
    end
    @title = formated.join(" ")
  end

end
