#require 'byebug'

class Dictionary

  def initialize
    @dictionary = {}
  end

  def add(entry)
    if entry.class == String
      @dictionary[entry] = nil
    else
      @dictionary = @dictionary.merge(entry)
    end
  end

  def include? (word)
    @dictionary.keys.include? word
  end

  def entries
    @dictionary
  end

  def keywords
    self.dictionary_word_array.sort
  end


  def dictionary_word_array
    list = self.entries
    key_list = list.keys
  end


  def find(search)
    key_list = self.dictionary_word_array
    matches = key_list.select {|i| partial_match(search,i)}
    search_result = {}
    matches.each {|word| search_result[word] = @dictionary[word]}
    search_result
  end

  def partial_match(str1, str2)
    range = (str1.length) - 1
    str2[0..range] == str1
  end



  def printable
    printable_strings = []
        keywords.each do |word|
            printable_strings << %Q([#{word}] "#{@dictionary[word]}")
        end
    printable_strings.join("\n")
  end


end
